variable "name" {
  type        = string
  description = "Project name"
}

variable "environment" {
  type        = string
  description = "Environment name"
  default     = "develop"
}

variable "resource_group_name" {
  type        = string
  description = "Resource group name"
}

variable "mysql_version" {
  type        = string
  description = "MySQL version"
  default     = "8.0.21"
}

variable "sku_name" {
  type        = string
  description = "MySQL SKU name"
  default     = "GP_Standard_D2ds_v4"  
}

variable "charset" {
  type        = string
  description = "MySQL Charset"
  default     = "utf8"  
}

variable "collation" {
  type        = string
  description = "MySQL Collation"
  default     = "utf8_unicode_ci"  
}

variable "root_username" {
  type        = string
  description = "Default root username"
  sensitive   = true
}

variable "root_password" {
  type        = string
  description = "Kubernetes service CIDR"
  sensitive   = true
}

variable "backup_retention_days" {
  type        = number
  description = "Backup retention days"
  default     = 7
}

variable "virtual_network_id" {
  type        = string
  description = "Virtual Network identifier"
}

variable "subnet_id" {
  type        = string
  description = "Subnet identifier"
}

variable "tags" {
  type        = map
  description = "Additional default tags"
  default     = {}
}
