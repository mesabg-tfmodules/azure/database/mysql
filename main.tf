resource "random_string" "id" {
  length  = 10
  lower   = true
  numeric = false
  special = false
  upper   = false
}

resource "azurerm_private_dns_zone" "this" {
  name                = "${var.name}.mysql.database.azure.com"
  resource_group_name = data.azurerm_resource_group.this.name

  tags                = local.tags
}

resource "azurerm_private_dns_zone_virtual_network_link" "this" {
  name                  = "${var.name}vnetzone.com"
  private_dns_zone_name = azurerm_private_dns_zone.this.name
  virtual_network_id    = var.virtual_network_id
  resource_group_name   = data.azurerm_resource_group.this.name

  tags                  = local.tags
}

resource "azurerm_mysql_flexible_server" "this" {
  name                    = "${var.name}-${random_string.id.result}"

  resource_group_name     = data.azurerm_resource_group.this.name
  location                = data.azurerm_resource_group.this.location
  administrator_login     = var.root_username
  administrator_password  = var.root_password
  backup_retention_days   = var.backup_retention_days
  delegated_subnet_id     = var.subnet_id
  private_dns_zone_id     = azurerm_private_dns_zone.this.id
  sku_name                = var.sku_name
  tags                    = local.tags

  lifecycle {
    ignore_changes = [
      zone,
      high_availability[0].standby_availability_zone,
    ]

    prevent_destroy = true
  }

  depends_on              = [
    azurerm_private_dns_zone_virtual_network_link.this,
  ]
}

resource "azurerm_mysql_flexible_server_configuration" "require_secure_transport" {
  name                = "require_secure_transport"
  resource_group_name = data.azurerm_resource_group.this.name
  server_name         = azurerm_mysql_flexible_server.this.name
  value               = "off"
}

# Default initial database
resource "azurerm_mysql_flexible_database" "this" {
  name                = var.name
  resource_group_name = data.azurerm_resource_group.this.name
  server_name         = azurerm_mysql_flexible_server.this.name
  charset             = var.charset
  collation           = var.collation

  # Prevent the possibility of accidental data loss
  lifecycle {
    prevent_destroy = true
  }
}
