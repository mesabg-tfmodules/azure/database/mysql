output "server_id" {
  value = azurerm_mysql_flexible_server.this.id
}

output "server_fqdm" {
  value = azurerm_mysql_flexible_server.this.fqdn
}

output "server_name" {
  value = azurerm_mysql_flexible_server.this.name
}

output "default_database_id" {
  value = azurerm_mysql_flexible_database.this.id
}

output "default_database_name" {
  value = azurerm_mysql_flexible_database.this.name
}
